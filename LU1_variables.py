# -*- coding: utf-8 -*-
"""
Created on Sun Mar  4 15:09:33 2018

@author: Ola
"""

############################ LEARNING UNIT 1 ################################

### EXERCISE 1 ###
stock="Google"
value=23.5
shares=110
action="buy"

print('I would like to', action, shares, 'shares in', stock)


### EXERCISE 2 ###
person1="Steve"
euros=100
person2= "Jobs"
number_stocks=5
stock2="Apple"

print(person1, 'needs to borrow', euros, 'euros from', person2,
      'in order to trade', number_stocks, 'stocks in', stock2)


### EXERCISE 3 ###
x=50
y=30
z=x*y

print(z)


### EXERCISE 4 ###
celsius=30
fahrenheit=celsius*9/5+32
kelvin=celsius+273.15

print(celsius, '°C)')
print(fahrenheit, '°F')
print(kelvin, 'K')
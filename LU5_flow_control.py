# -*- coding: utf-8 -*-
"""
Created on Sun Mar  4 16:06:36 2018

@author: Ola
"""

##################### Function/variable as a condition ########################
    
def return_true():
    return True
    
if return_true():
    print('hi guys')
    
val = return_true()

if val:
    print('hi girls')
    
############################ FOR statements ##################################
    
a_list = [1,2,3]

for num in a_list:
    print(num) #Each value is printed separately


############################# Learing Unit 5 #################################
    
# Exercise 1: What is the boolean value of the following
bool(1.1) #True
bool(0.1) #True
bool(None) #False
bool('') #False
bool(0) #False


# Exercise 2:  Write a function
# That takes a single argument which is a ticker symbol
# Returns True if the ticker symbol is “APPL”
# Returns False otherwise

def ticker_test(ticker):
    if ticker == 'APPL':
        return True
    else:
        return False
    
print(ticker_test('lll'))


# Exercise 3:  Write a function
# That takes a single argument which is your gender
# Returns “You like pink” if the gender is ‘male’ and 
# “You hate pink” if the gender is ‘female’

def gender_test(gender):
    if gender == 'female':
        print('You like pink')
    else:
        print('You hate pink')
print(xxx)
    
    
    
gender_test('female')


# Exercise 5: Write a function
# That takes two arguments
   # the age of your mother
   # the age of your father.
# If the age is the same return the sum of both ages
# If they are different return the difference between ages 
    
def equal_ages(mom, dad):
    if mom == dad:
        print(mom+dad)
        return mom+dad
    else:
        print(dad-mom)
        return dad-mom
    
equal_ages(33,33)
equal_ages(33,43)  
    

# Exercise 6: Write a function
# Write a function that takes a dictionary and a list as 
# an argument. If the dictionary has as many items as 
# the size of the list, return “awesome”. If they
# have different values return “that is sad”. 

def length(d,l):
    if len(d) == len(l):
        print('awesome')
        return True
    else:
        print('so sad')
        return False

my_dd={'a': 1, 'b': 2}
my_ll=[3,4]
my_lll=[3,4,7]

length(my_dd, my_ll)

# Exercise 7: Define a dictionary that maps the ticker symbol to today’s 
# stock price for the following companies.
# Write 3 different loops that loop over this dictionary that
# Print the keys one at a time
# Print the values one at a time
# Print a string that prints “the stock price of <company> is <value>” where
# <company> and <value> are the company stock symbols and stock prices respectively

three_stocks = {'Apple': 100, 'Google': 120, 'Facebook': 90}

for value in three_stocks:    #printing values
    print(three_stocks[value])

for key in three_stocks:     #printing keys
    print(key)

# Exercise 8
tuplee = (1,2,3)
listt = [4,5,6]
dictt = {'a': 7, 'b': 8}

for i in tuplee:
    print(i)

for i in listt:
    print(i)

for i in dictt:
    print(dictt[i])


# Exercise 9
# That takes a single list as it’s argument
# Returns a new list that multiplies each of the entries of the input list by 2   
 
# Exercise 10
def double_it(d):
    new_d = {}
    for i in d:
        new_d[i] = 2*d[i]
    print(new_d)
    return new_d
    
    
    
    
    
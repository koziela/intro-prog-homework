# -*- coding: utf-8 -*-
"""
Created on Sun Mar  4 15:47:01 2018

@author: Ola
"""

############################ LEARNING UNIT 4 ################################

#### EXERCISE 1 ### 
# Tuples are not suppose to be changed - they are imutable! 


#### EXERCISE 2 ### 
apple = (101, 102, 103, 104, 105) #creating tuple


#### EXERCISE 3 ### 
# Tuples are not suppose to be sorted, because they are imutable. 
# You can find ways - turn it into a list, sorted, turn it into a tuple. 


#### EXERCISE 4 ### 
apple_list = [101, 102, 103, 104, 105]
apple_list.append(106)
print(apple_list) # 106 is added at the end


#### EXERCISE 6 ### 
#Tuples and lists are equal only if have the same type and order


#### EXERCISE 7 ### 
t = (1, 2, 3)
l = list(t) #Turning tuple into list
print(type(l), l) 


#### EXERCISE 8 ###
d = {'a':1, 'b':2}

print(d.keys()) # Printing keys 
d.values() # Printing values
d.items() # Printing items


### EXERCISE 9 ###
my_d={}
my_d['First'] = 1 # Adding new items to empty dictionary
my_d['Second'] = 2


#### EXERCISE 10 ### 
dicto = {'One': 100, 'Two': 200} 
sth = 'One'
print(dicto[sth]) #Print 100
#Flexible access to the dictionary


#### EXERCISE 11 ### 
# Creating Lists
list_one = []
list_two = list()

# Creating dictionaries
dict_one = {}
dict_two = dict()

# Creating tuples
t_one = ()
t_two = tuple()


#### EXERCISE 12 ### 
del dicto['One'] #Removing items
dicto.pop('One')


#### EXERCISE 13 ### 
def list_maker(my_list=[1,2,3]):
    my_list.append('homie')
    return my_list

moja_lista = list_maker()
print(moja_lista)


#### EXERCISE 14 ### 
def average_stocks(stock1, stock2, stock3):
    r=(stock1+stock2+stock3)/3
    return r

average_stocks(100,200,150)


def average_stocks2(stock_list=list()): #With list
    r=sum(stock_list)/3
    return r

average_stocks2([100,200,150])

moja_lista2=[200,200,200]
average_stocks2(moja_lista2)
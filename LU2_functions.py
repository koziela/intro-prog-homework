# -*- coding: utf-8 -*-
"""
Created on Sun Mar  4 15:12:19 2018

@author: Ola
"""

############################ LEARNING UNIT 2 #################################

### EXERCISE 1 ###
def power(a,b) :    
    return a**b

power(2,3)


### EXERCISE 2 ###
def equal_or_not(a,b) :    
    return a == b

equal_or_not(2,3)
equal_or_not(2,2)


### EXERCISE 3 ###
def pythagoras(a,b) : 
    first = a**2
    second = b**2
    h_squared = first + second
    h = h_squared**(1/2)
    return h

pythagoras(2,2)


### EXERCISE 4 ###
def EUR(BTC) :    
    return BTC*7023.24

def LTC(BTC) :    
    return BTC*54.31

def ETH(BTC) :    
    return BTC*10.224998


### EXERCISE 5 ###
def what(a,b) :    
    c=a/b
    return
# Python functions always return something. If it is not defined by the user,
# returns None
    

### EXERCISE 6 ###
# Print displays it's arguments on the screen and return is used to return
# a value from a function. 


### EXERCISE 7 ###
# The REPL prints [OUT] when the expression on the REPL evaluates to not null
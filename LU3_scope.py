# -*- coding: utf-8 -*-
"""
Created on Sun Mar  4 15:32:42 2018

@author: Ola
"""

############################ LEARNING UNIT 3 #################################

### EXERCISE 1 ###
#Scope is the area in which variable can be used
#Local scope is the scope within the function
#Global scope is the scope that all functions can use them in the file


### EXERCISE 3 ###
print('I like', var)
var = 3
#If assignement of a is after print(a), it return: name 'a' is not defined


### EXERCISE 4 ###
g=2
def double():    
    r=g*2
    return r

two_g = double()

print(g) #Print g = 2
print(two_g) #Print return of function = 4
print(r) #Error cuz r is in local scope


### EXERCISE 7 ###
x='moje'
y='twoje'
def cos(x,y):
    x='ja'
    y='ty'
    print(x,y)
cos(x,y) #Print 'ja' and 'ty' from local scope of function
print(x,y) #Print 'moje' and 'twoje' form global scope


### EXERCISE 9-10 ###
first_name = 'Kasia'
last_name = 'Olek'

def names(first_name ='Ola', last_name ='Koziel'):    
    print(first_name, last_name)
    
names()
names(first_name ='Zuzia')
names('Basia', 'Pietrzak')
names(first_name = first_name,) #It takes 'Kasia' from global


### EXERCISE 11 ###
def hello(your_message ='Good Morning', name = ''):
    print(your_message,name)
    
hello()
hello(name ='Ricardo')
hello(your_message = 'You know nothing', name ='John Snow')

